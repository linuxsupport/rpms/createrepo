GIT_NAME             = $(shell basename `git rev-parse --show-toplevel`)
GIT_VERSION          = $(shell git describe --tags | sed s/v// | cut -d "-" -f 1)
GIT_RELEASE          = $(shell git describe --tags | sed s/v// | cut -d "-" -f 2)
SPECFILE             = $(shell find -maxdepth 1 -type f -name *.spec)
SPECFILE_NAME        = $(shell awk '$$1 == "Name:"     { print $$2 }' $(SPECFILE) )
SPECFILE_VERSION     = $(shell awk '$$1 == "Version:"  { print $$2 }' $(SPECFILE) )
SPECFILE_RELEASE     = $(shell awk '$$1 == "Release:"  { print $$2 }' $(SPECFILE) )
GITLAB_ORG           = "linuxsupport/rpms"
KOJI_TAG             = "linuxsupport7"
DIST_TAG             = ".el7.cern"

sources:
	tar -zcvf $(SPECFILE_NAME)-$(SPECFILE_VERSION).tgz --exclude-vcs --transform 's,^code/,$(SPECFILE_NAME)-$(SPECFILE_VERSION)/,' code/*

build:
	koji build --nowait $(KOJI_TAG) git+ssh://git@gitlab.cern.ch:7999/$(GITLAB_ORG)/$(SPECFILE_NAME).git#v$(VERSION)-$(RELEASE)

scratch:
	koji build --scratch $(KOJI_TAG) git+ssh://git@gitlab.cern.ch:7999/$(GITLAB_ORG)/$(SPECFILE_NAME).git#v$(VERSION)-$(RELEASE)

rpm: sources
	rpmbuild -bb --define "dist $(DIST_TAG)" --define "_topdir $(PWD)/build" --define '_sourcedir $(PWD)/sources' $(SPECFILE)

srpm: sources
	rpmbuild -bs --define "dist $(DIST_TAG)" --define "_topdir $(PWD)/build" --define '_sourcedir $(PWD)/sources' $(SPECFILE)
